//
//  ViewController.h
//  SportlerPlus
//
//  Created by Bjarne on 29.09.20.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController
@property (weak) IBOutlet WKWebView *webView;
@end

