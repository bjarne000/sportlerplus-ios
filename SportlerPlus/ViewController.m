//
//  ViewController.m
//  SportlerPlus
//
//  Created by Bjarne on 29.09.20.
//

#import "ViewController.h"

@interface WKWebView(SynchronousEvaluateJavaScript)
- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script;
@end

@implementation WKWebView(SynchronousEvaluateJavaScript)

- (NSString *)stringByEvaluatingJavaScriptFromString:(NSString *)script
{
    __block NSString *resultString = nil;
    __block BOOL finished = NO;

    [self evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error == nil) {
            if (result != nil) {
                resultString = [NSString stringWithFormat:@"%@", result];
            }
        } else {
            NSLog(@"evaluateJavaScript error : %@", error.localizedDescription);
        }
        finished = YES;
    }];

    while (!finished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }

    return resultString;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *httpSource = @"https://app.sportlerplus.com/";
    // set user agent
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Mozilla/Whatever version 913.6.beta", @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    // load webview
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:httpSource]]];
    
}

/*
 when back button defined
- (IBAction)backButtonTapped:(id)sender {
   [self.webView goBack];
}
 */

@end
